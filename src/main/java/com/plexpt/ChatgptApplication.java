package com.plexpt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName ChatgptApplication
 * @Description TODO
 * @Author jq
 * @Date 2023/5/10 11:54
 */
@SpringBootApplication
public class ChatgptApplication {


    public static void main(String[] args) {
        SpringApplication.run(ChatgptApplication.class, args);
        System.out.println("===============================启动完成===========================");
    }

}
