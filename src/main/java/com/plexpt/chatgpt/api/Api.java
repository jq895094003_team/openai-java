package com.plexpt.chatgpt.api;

import com.plexpt.chatgpt.entity.billing.CreditGrantsResponse;
import com.plexpt.chatgpt.entity.billing.SubscriptionData;
import com.plexpt.chatgpt.entity.billing.UseageResponse;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;

import com.plexpt.chatgpt.entity.image.ImageEditRequest;
import com.plexpt.chatgpt.entity.image.ImageGenRequest;
import com.plexpt.chatgpt.entity.image.ImageResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 *
 */
public interface Api {

    String DEFAULT_API_HOST = "https://api.openai.com/";


    /**
     * chat
     */
    @POST("v1/chat/completions")
    Single<ChatCompletionResponse> chatCompletion(@Body ChatCompletion chatCompletion);

    /**
     * 根据描述生成一战图片
     * @param imageEditRequest
     * @return
     */
    @POST("v1/images/generations")
    Single<ImageResponse> imageGen(@Body ImageGenRequest imageEditRequest);

    /**
     * 修改一张图片
     * @param imageEditRequest
     */
    @POST("v1/images/edits")
    Single<ImageResponse> imagesEdits(@Body ImageEditRequest imageEditRequest);


    /**
     * 余额查询
     */
    @GET("dashboard/billing/credit_grants")
    Single<CreditGrantsResponse> creditGrants();

    /**
     * 余额查询
     */
    @GET("v1/dashboard/billing/subscription")
    Single<SubscriptionData> subscription();

    /**
     * 余额查询
     */
    @GET("v1/dashboard/billing/usage")
    Single<UseageResponse> usage(@Query("start_date") String startDate,
                                 @Query("end_date") String endDate);


}
