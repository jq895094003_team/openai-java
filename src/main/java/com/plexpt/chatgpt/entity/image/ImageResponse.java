package com.plexpt.chatgpt.entity.image;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @ClassName openai图片 答案类
 * @Description TODO
 * @Author Jq
 * @Date 2023/5/11 11:03
 */
@Data
public class ImageResponse {

    private long created;


    private List<Data> data;


    /**
     * A nested class that represents an image object returned in the API response.
     */
    @lombok.Data
    public static class Data {
        private String url;

        private String b64_json;

    }


}
