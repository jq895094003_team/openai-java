package com.plexpt.chatgpt.entity.image;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName 修改图片 request参数类
 * @Description TODO
 * @Author Jq
 * @Date 2023/5/11 10:37
 */
@Slf4j
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageEditRequest {
    /**
     * 图片（必填）
     * 要编辑的图像。必须是有效的 PNG 文件，小于 4MB，并且是正方形。如果未提供蒙版，则图像必须具有透明度，该透明度将用作蒙版。
     */
    private String image;

    /**
     * 附加图片
     * 一个附加图像，其完全透明的区域（例如，alpha为零）指示应编辑的位置。必须是有效的 PNG 文件，小于 4MB，并且尺寸与 相同。
     */
    private String mask;

    /**
     * 要求描述（必填）
     * 所需图像的文本描述。最大长度为 1000 个字符。
     */
    private String prompt;

    /**
     * 要生成的图像数。必须介于 1 和 10 之间
     */
    @Builder.Default
    private Integer n = 1;

    /**
     * 生成的图像的大小。必须是 256x256,512x512,1024x1024
     * @see ImageSizeEnum
     */
    @Builder.Default
    private String size = ImageSizeEnum.s256.getSize();

    /**
     * 返回生成的图像的格式。必须是 url,b64_json
     * @see ImageResponseFormatEnum
     */
    @Builder.Default
    @JsonProperty("response_format")
    private String responseFormat = ImageResponseFormatEnum.url.getFormat();

    /**
     * 代表最终用户的唯一标识符，可帮助 OpenAI 监控和检测滥用行为。
     */
    private String user;


}
