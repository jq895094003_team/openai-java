package com.plexpt.chatgpt.entity.image;

/**
 * @ClassName 返回生成的图像的格式
 * @Description TODO
 * @Author Jq
 * @Date 2023/5/11 11:00
 */
public enum ImageResponseFormatEnum {

    /**
     * 返回url
     */
    url("url"),
    /**
     * 返回base64的文件
     */
    base64("b64_json");
    private String format;

    ImageResponseFormatEnum(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}
