package com.plexpt.chatgpt.entity.image;

/**
 * @ClassName 图片分辨率
 * @Description TODO
 * @Author Jq
 * @Date 2023/5/11 10:59
 */
public enum ImageSizeEnum {
    s256("256x256"),
    s512("512x512"),
    s1024("1024x1024")
    ;
    private String size;

    ImageSizeEnum(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
