package com.plexpt.chatgpt.entity.image;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName 按需求生成图片参数类
 * @Description TODO
 * @Author Jq
 * @Date 2023/5/11 10:58
 */
@Slf4j
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageGenRequest {


    /**
     * 要求描述（必填）
     * 所需图像的文本描述。最大长度为 1000 个字符。
     */
    private String prompt;

    /**
     * 要生成的图像数。必须介于 1 和 10 之间
     */
    @Builder.Default
    private Integer n = 1;

    /**
     * 生成的图像的大小。必须是 256x256,512x512,1024x1024
     * @see ImageSizeEnum
     */
    @Builder.Default
    private String size = ImageSizeEnum.s1024.getSize();

    /**
     * 返回生成的图像的格式。必须是 url,b64_json
     * @see ImageResponseFormatEnum
     */
    @Builder.Default
    @JsonProperty("response_format")
    private String responseFormat = ImageResponseFormatEnum.url.getFormat();

    /**
     * 代表最终用户的唯一标识符，可帮助 OpenAI 监控和检测滥用行为。
     */
    private String user;
}
