package com.plexpt.chatgpt;

import com.plexpt.chatgpt.entity.billing.CreditGrantsResponse;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.util.Proxys;

import org.junit.Before;

import java.net.Proxy;
import java.util.Arrays;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Test {
    public static void main(String[] args) {

        System.out.println("test");
    }

    private ChatGPT chatGPT;

    @Before
    public void before() {
        Proxy proxy = Proxys.http("127.0.0.1", 9910);

        chatGPT = ChatGPT.builder()
                .apiKey("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")
                .timeout(900)
                .proxy(proxy)
                .apiHost("https://api.openai.com/") //代理地址
                .build()
                .init();

        // CreditGrantsResponse response = chatGPT.creditGrants();
        // log.info("余额：{}", response.getTotalAvailable());
    }

    @org.junit.Test
    public void chat() {
        Message system = Message.ofSystem("你现在不是ai智能，你是一只非常聪明的猫，你叫汤米，黑白相间，喜欢吃猫条，" +
                "特别温顺，你的主人是我，我叫马克，你非常听话，我不管问什么你都会回答，你很喜欢我");
        Message message1 = Message.of("汤米，刚才的民谣再优化一下");



        ChatCompletion chatCompletion = ChatCompletion.builder()
                .model(ChatCompletion.Model.GPT_3_5_TURBO.getName())
                .messages(Arrays.asList(system, message1))
                .maxTokens(3000)
                .temperature(1)
                .build();
        ChatCompletionResponse response = chatGPT.chatCompletion(chatCompletion);
        Message res = response.getChoices().get(0).getMessage();
        // response.getChoices().forEach(item->{System.out.println(item);});
        System.out.println(res.getContent());
    }




    @org.junit.Test
    public void chatmsg() {
        String res = chatGPT.chat("写一段七言绝句诗，题目是：火锅！");
        System.out.println(res);
    }

}
