package com.plexpt.chatgpt;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.util.ByteUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.entity.image.*;
import com.plexpt.chatgpt.util.Proxys;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.springframework.util.Base64Utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.net.Proxy;
import java.nio.charset.Charset;
import java.util.Arrays;

/**
 *
 * @author Jq
 */
@Slf4j
public class ImageTest {
    public static void main(String[] args) {

        System.out.println("test");
    }

    private ImageAi imageAi;

    @Before
    public void before() {
        Proxy proxy = Proxys.http("127.0.0.1", 9910);

        imageAi = ImageAi.builder()
                .apiKey("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")
                .timeout(900)
                .proxy(proxy)
                .apiHost("https://api.openai.com/") //代理地址
                .build()
                .init();

        // CreditGrantsResponse response = chatGPT.creditGrants();
        // log.info("余额：{}", response.getTotalAvailable());
    }

    @org.junit.Test
    public void genImageUrl() {

        String prompt = "生成一个头像，要求是独一无二，别具一格";


        ImageGenRequest imageGenRequest = ImageGenRequest.builder()
                .prompt(prompt)
                .size(ImageSizeEnum.s1024.getSize())
                .n(1)
                .build();
        ImageResponse response = imageAi.imageGen(imageGenRequest);
        // response.getChoices().forEach(item->{System.out.println(item);});
        System.out.println("生成图片请求成功！相应如下：");
        System.out.println(ObjectUtil.toString(response));

        //存储文件
        for (int i = 0; i < response.getData().size(); i++) {
            //文件地址
            String fileHttpUrl = response.getData().get(i).getUrl();
            //文件名称
            String fileName = System.currentTimeMillis() + i +".png";
            //本地文件存放地址
            String localFilePath = "static/img/gen/"+fileName;

            HttpUtil.downloadFileFromUrl(fileHttpUrl,localFilePath);
        }


    }

    @org.junit.Test
    public void genImageBase64() {

        String prompt = "一只白色暹罗猫的特写工作室摄影肖像，它看起来好奇，背光的耳朵";


        ImageGenRequest imageGenRequest = ImageGenRequest.builder()
                .prompt(prompt)
                .responseFormat(ImageResponseFormatEnum.base64.getFormat())
                .size(ImageSizeEnum.s256.getSize())
                .build();
        ImageResponse response = imageAi.imageGen(imageGenRequest);
        for (int i = 0; i < response.getData().size(); i++) {
            String fileBase64Str = response.getData().get(i).getB64_json();
            //文件名称
            String fileName = System.currentTimeMillis() + i +".png";
            //本地文件存放地址
            String localFilePath = "static/img/gen/"+fileName;
            //base64 转文件并且存储本地
            Base64.decodeToFile(fileBase64Str,FileUtil.file(localFilePath));


        }


        System.out.println("生成图片请求成功！相应如下：");

        System.out.println(ObjectUtil.toString(response));
    }

    @org.junit.Test
    public void editImageUrl() {



        String prompt = "把图片中的蛇颜色变成绿色的";

        String fileUrl = "https://img2.baidu.com/it/u=941861093,2521433863&fm=253&fmt=auto&app=138&f=JPG?w=256&h=256";

        ImageEditRequest imageEditRequest = ImageEditRequest.builder()
                .image(fileUrl)
                .prompt(prompt)
                .size(ImageSizeEnum.s256.getSize())
                .build();

        ImageResponse response = imageAi.imagesEdits(imageEditRequest);

        System.out.println("生成图片请求成功！相应如下：");

        System.out.println(ObjectUtil.toString(response));

        //存储文件
        for (int i = 0; i < response.getData().size(); i++) {
            //文件地址
            String fileHttpUrl = response.getData().get(i).getUrl();
            //文件名称
            String fileName = System.currentTimeMillis() + i +".png";
            //本地文件存放地址
            String localFilePath = "static/img/edit/"+fileName;

            HttpUtil.downloadFileFromUrl(fileHttpUrl,localFilePath);
        }


    }

    @org.junit.Test
    public void editImageBase64() {

        String prompt = "把图片中的蛇颜色变成绿色的";


        ImageEditRequest imageEditRequest = ImageEditRequest.builder()
                .image("")
                .responseFormat(ImageResponseFormatEnum.base64.getFormat())
                .prompt(prompt)
                .size(ImageSizeEnum.s256.getSize())
                .build();
        ImageResponse response = imageAi.imagesEdits(imageEditRequest);
        // response.getChoices().forEach(item->{System.out.println(item);});
        System.out.println("生成图片请求成功！相应如下：");

        System.out.println(ObjectUtil.toString(response));
    }



}
