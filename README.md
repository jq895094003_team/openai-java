<h1 style="text-align: center; color: hotpink; -webkit-animation: rainbow 5s infinite; -moz-animation: rainbow 5s infinite; -o-animation: rainbow 5s infinite; animation: rainbow 5s infinite;">ChatGPT Openai Java Demo</h1>


OpenAI ChatGPT 的SDK 和 spring-sse demo。觉得不错请右上角Star

本项目基于PlexPt-[chatgpt-java](https://github.com/PlexPt/chatgpt-java)
<hr/>在此基础上，新增了spring-sse 和 上下文携带聊天记录的demo

详见[SseController](src/main/java/com/plexpt/chatgpt/controller/SseController.java)
<hr/>

新增了[openai-Image generation](https://platform.openai.com/docs/guides/images/introduction) 的java代码调用示例
图片生成和修改图片

详见[ImageTest](src/test/java/com/plexpt/chatgpt/ImageTest.java)




# 功能特性

|    功能     |   特性   |
| :---------: | :------: |
|   GPT 3.5   |   支持   |
|   GPT 4.0   |   支持   |
| GPT 4.0-32k |   支持   |
|  流式对话   |   支持   |
| 阻塞式对话  |   支持   |
|    前端     |    无    |
|   上下文    |   支持   |
|  计算Token  | 即将支持 |
|  多KEY轮询  |   支持   |
|    代理     |   支持   |
|  反向代理   |   支持   |
|  spring-sse   |   支持（新增）   |
|  图片生成   |   支持（新增）   |



### 最简使用

也可以使用这个类进行测试 [ConsoleChatGPT](src/main/java/com/plexpt/chatgpt/ConsoleChatGPT.java)

```java
      //国内需要代理
      Proxy proxy = Proxys.http("127.0.0.1", 1081);
     //socks5 代理
    // Proxy proxy = Proxys.socks5("127.0.0.1", 1080);

      ChatGPT chatGPT = ChatGPT.builder()
                .apiKey("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")
                .proxy(proxy)
                .apiHost("https://api.openai.com/") //反向代理地址
                .build()
                .init();
                
        String res = chatGPT.chat("写一段七言绝句诗，题目是：火锅！");
        System.out.println(res);

```


### 进阶使用

```java
      //国内需要代理 国外不需要
      Proxy proxy = Proxys.http("127.0.0.1", 1080);

      ChatGPT chatGPT = ChatGPT.builder()
                .apiKey("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")
                .proxy(proxy)
                .timeout(900)
                .apiHost("https://api.openai.com/") //反向代理地址
                .build()
                .init();
     
        Message system = Message.ofSystem("你现在是一个诗人，专门写七言绝句");
        Message message = Message.of("写一段七言绝句诗，题目是：火锅！");

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .model(ChatCompletion.Model.GPT_3_5_TURBO.getName())
                .messages(Arrays.asList(system, message))
                .maxTokens(3000)
                .temperature(0.9)
                .build();
        ChatCompletionResponse response = chatGPT.chatCompletion(chatCompletion);
        Message res = response.getChoices().get(0).getMessage();
        System.out.println(res);

```

### 流式使用

```java
      //国内需要代理 国外不需要
      Proxy proxy = Proxys.http("127.0.0.1", 1080);

      ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .timeout(600)
                .apiKey("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")
                .proxy(proxy)
                .apiHost("https://api.openai.com/")
                .build()
                .init();

                
        ConsoleStreamListener listener = new ConsoleStreamListener();
        Message message = Message.of("写一段七言绝句诗，题目是：火锅！");
        ChatCompletion chatCompletion = ChatCompletion.builder()
                .messages(Arrays.asList(message))
                .build();
        chatGPTStream.streamChatCompletion(chatCompletion, listener);

```

### 流式配合Spring SseEmitter使用

参考 [SseStreamListener](src/main/java/com/plexpt/chatgpt/listener/SseStreamListener.java)

```java
  

    @GetMapping("/chat/sse")
    @CrossOrigin
    public SseEmitter sseEmitter(String prompt) {
       //国内需要代理 国外不需要
       Proxy proxy = Proxys.http("127.0.0.1", 1080);

       ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .timeout(600)
                .apiKey("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")
                .proxy(proxy)
                .apiHost("https://api.openai.com/")
                .build()
                .init();
        
        SseEmitter sseEmitter = new SseEmitter(-1L);

        SseStreamListener listener = new SseStreamListener(sseEmitter);
        Message message = Message.of(prompt);

        listener.setOnComplate(msg -> {
            //回答完成，可以做一些事情
        });
        chatGPTStream.streamChatCompletion(Arrays.asList(message), listener);


        return sseEmitter;
    }

```



## 多KEY自动轮询

只需替换chatGPT构造部分

```
chatGPT = ChatGPT.builder()
        .apiKeyList(
               // 从数据库或其他地方取出多个KEY
                Arrays.asList("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",
                        "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",
                        "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",
                        "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",
                        ))
        .timeout(900)
        .proxy(proxy)
        .apiHost("https://api.openai.com/") //代理地址
        .build()
        .init();
```

## 上下文

参考  [ChatContextHolder.java](src/main/java/com/plexpt/chatgpt/util/ChatContextHolder.java) 



# 常见问题

|                              问                              |                              答                              |
| :----------------------------------------------------------: | :----------------------------------------------------------: |
|                         KEY从哪来？                          | 手动注册生成：ai.com(需要海外手机号)、或者成品独享帐号：[购买](https://fk.fq.mk/?code=YT0xJmI9Mg%3D%3D) |
|                        哪些地区不能用                        | **以下国家IP不支持使用：中国(包含港澳台) 俄罗斯 乌克兰 阿富汗 白俄罗斯 委内瑞拉 伊朗 埃及!!** |
|                         有封号风险吗                         |              充值的没有。你免费白嫖不封你封谁。              |
|                  我是尊贵的Plus会员，能用吗                  |             能用，照封不误。PLUS调用API没啥区别              |
|                        GPT4.0 怎么用                         |          申请 https://openai.com/waitlist/gpt-4-api          |
|                  api.openai.com ping不通？                   |                   禁ping，用curl测试连通性                   |
|                          显示超时？                          |                        IP不好，换个IP                        |
| 显示`Your access was terminated due to violation of our policies`... |                       你号没了，下一个                       |
| 显示`That model is currently overloaded with other requests. You can retry your request` |                   模型过载，官方炸了，重试                   |
|                          如何充值？                          |                   用国外信用卡，国内的不行                   |
|                         返回http 401                         |                     API 密钥写错了/没写                      |
|                         返回http 429                         |            请求超速了，或者官方超载了。充钱可解决            |
|                         返回http 500                         |                          服务器炸了                          |
|                                                              |                                                              |

---





### 注册教程

https://juejin.cn/post/7173447848292253704

https://mirror.xyz/boxchen.eth/9O9CSqyKDj4BKUIil7NC1Sa1LJM-3hsPqaeW_QjfFBc

####  [ChatGPT中文使用指南](https://github.com/PlexPt/awesome-chatgpt-prompts-zh)
####  感谢 [PlexPt](https://github.com/PlexPt)